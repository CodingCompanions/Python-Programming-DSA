def insertionSort(seq):
	for endpos in range(len(seq)):
		pos=endpos
		while(pos>0 and seq[pos]<seq[pos-1]):
			(seq[pos],seq[pos-1])=(seq[pos-1],seq[pos])
			pos=pos-1
