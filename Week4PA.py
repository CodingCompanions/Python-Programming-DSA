#Week 4 Programming Assignment
#Omkar Nath Singh

#Question 1
def orangecap(d):

	a={}
	for match in d.keys():
		for player in d[match]:
			if player not in a:
				a[player] = d[match][player]
			else :
				a[player] += d[match][player]
	big = 0
	for player in a.keys():
		if big < a[player]:
			big = a[player]
			capholder = player
  return (capholder,big)

#Question 2
def multpoly(p1,p2):
    result=[]
    for i in range(len(p1)):
        for j in range(len(p2)):
            temp=(p1[i][0]*p2[j][0],p1[i][1]+p2[j][1])
            result.append(temp)
            
    result=removezero(result)
    result=addpoly(result[:1],result[1:])
    result.sort(reverse=True)
    return(result)

def removezero(m):
    l=[]
    for i in range(len(m)):
        if(m[i][0]!=0):
            l.append(m[i])
    return(l)

def addpoly(p1,p2):
    m=p1+p2
    result=[]
    donepower=[]
    for i in range(len(m)-1):
        for j in range(i+1,len(m)):
            if((m[i][1]==m[j][1]) and (m[i][0]+m[j][0])==0):
                donepower.append(m[i][1])
            elif(m[i][1]==m[j][1]):
                temp=(m[i][0]+m[j][0],m[i][1])
                result.append(temp)
                donepower.append(m[i][1])

    for i in range(len(m)):
        if(m[i][1] not in donepower):
            result.append(m[i])

    return(sorted(result))

print(multpoly([(1,1),(-1,0)],[(1,2),(1,1),(1,0)]))
