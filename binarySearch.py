def binarySearch(seq,v,l,r):
  if(r==l):
    return(False)
  
  mid=(l+r)//2
  
  if(v==seq[mid]):
    return(True)
  
  if(v<seq[mid]):
    return(binarySearch(seq,v,l,mid))
  else:
    return(binarySearch(seq,v,mid+1,r))
