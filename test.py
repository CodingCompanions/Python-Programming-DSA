def subsequence(l1,l2):
 for i in range(len(l1)):
  if(l1[i] not in l2):
   return(False)
  if(l1[i] in l2):
   l2.remove(l1[i])
 return(True)