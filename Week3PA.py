#Week 3 Programming Assignment
#Omkar Nath Singh

#Question 1
def descending(l):
    for i in range(len(l)-1):
        if(l[i]<l[i+1]):
            return False;
    return True;


#Question 2
def alternating(l):
    for i in range(len(l)-2):
        if(l[i]==l[i+1]):
            return False

        if(l[i]<l[i+1]):
            if(l[i+1]<=l[i+2]):
                return False

        if(l[i]>l[i+1]):
            if(l[i+1]>=l[i+2]):
                return False
    return True


#Question 3
def matmult(m1,m2):
    m3=[]
    for i in range(len(m1)):
        m4=[]
        for j in range(len(m2[0])):
            temp=0
            for k in range(len(m1[0])):
                temp+=m1[i][k]*m2[k][j]
            m4.append(temp)
        m3.append(m4)
    return m3
