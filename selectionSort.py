def selectionSort(l):
  for start in range(len(l)):
    minpos=start
    for i in range(start,len(l)):
      if l[i]<l[minpos]:
        minpos=i
    (l[start],l[minpos])=(l[minpos],l[start])

l=list(range(100,1,-1))
print(l)
selectionSort(l)
print(l)
